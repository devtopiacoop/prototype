//File: controllers/users.js
var mongoose 	= require('mongoose');
var usersModel  = mongoose.model('users');

// POST - New user
exports.addUser = function(req, res) {
	console.log('POST');
	console.log(req.body);
	var user = new usersModel();
    user.nick       	= req.body.nick;
    //user.email      	= req.body.email;
    //user.settings   	= req.body.settings;
    //user.active     	= req.body.active;
    //user.hashedPassword = user.encryptPassword(req.body.password);

	user.save(function(err, user) {
		if(err) return res.status(500).send(err.message);
    	res.status(200).jsonp(user);
	});
};

//GET - Return all users in the DB
exports.findAllUsers = function(req, res) {
	usersModel.find(function(err, users) {
    	if(err) res.status(500).send(err.message);

    	console.log('GET /users')
		res.status(200).jsonp(users);
	});
};

//GET - Return a user with specified ID
exports.findUserById = function(req, res) {
	usersModel.findById(req.params.id, function(err, user) {
    	if(err) return res.status(500).send(err.message);

    	console.log('GET /users/' + req.params.id);
		res.status(200).jsonp(user);
	});
};

//GET - Return a user score with specified ID
exports.findUserScore = function(req, res) {
	// TODO
};

//PUT - Update a register already exists
exports.updateUser = function(req, res) {
	// TODO
}

//PUT - Update a register already exists
exports.addUserScore = function(req, res) {
	// TODO
};

//PUT - Update a register already exists
exports.addRewardToUser = function(req, res) {
	// TODO
};

//PUT - Update a register already exists
exports.removeRewardFromUser = function(req, res) {
	
};

//GET - Return a user with specified ID
exports.getUserRewards = function(req, res) {
	
};

//DELETE - Delete a user with specified ID
exports.deleteUser = function(req, res) {
	usersModel.findById(req.params.id, function(err, user) {
		user.remove(function(err) {
			if(err) return res.status(500).send(err.message);
      		res.status(200);
		})
	});
};