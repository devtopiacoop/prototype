//File: controllers/planets.js
var mongoose 		= require('mongoose');
var random 			= require('mongoose-simple-random');
var planetsModel  	= mongoose.model('planets');
var config          = require("../config");


//GET - Return all planets in the DB
exports.findAllPlanets = function(req, res) {
	planetsModel.find(function(err, planets) {
    	if(err) res.status(500).send(err.message);

    	console.log('GET /planets')
		res.status(200).jsonp(planets);
	});
};

//GET - Return all planets in the DB
exports.findPlanetById = function(req, res) {

};


// Find "limit" random documents (defaults to array of 1) 
exports.findRandomPlanets = function(req, res) {
	var filter = {};
	var fields = {};
	var options = { limit: req.params.n?req.params.n:config.PLANETS_PER_MATCH };
	planetsModel.findRandom(filter, fields, options, function(err, planets) {
		if(err) return res.status(500).send(err.message);
		console.log('GET random planets');
		res.status(200).jsonp(planets);
	});
};

//POST - Insert a new planet in the DB
exports.addPlanet = function(req, res) {
	console.log('POST addPlanet');
	console.log(req.body);
	var result;

	var planet = new planetsModel({
		points: 		req.body.points,
		size: 			req.body.size, // size in pixels
		imagePath: 	  	req.body.path
	});

	planet.save(function(err,planet) {
		if(err) return res.status(500).send(err.message);
		res.status(200).jsonp(planet);
	});

};


//PUT - Update a register already exists
exports.updatePlanet = function(req, res) {
	//TODO
};

//DELETE - Delete a planet with specified ID
exports.deletePlanet = function(req, res) {
	planetsModel.findById(req.params.id, function(err, planet) {
		planet.remove(function(err) {
			if(err) return res.status(500).send(err.message);
      		res.status(200);
		})
	});
};
