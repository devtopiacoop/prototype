//File: controllers/rewards.js
var mongoose 		= require('mongoose');
var rewardsModel  	= mongoose.model('rewards');

//GET - Return all rewards in the DB
exports.findAllRewards = function(req, res) {
	rewardsModel.find(function(err, rewards) {
    	if(err) res.status(500).send(err.message);

    	console.log('GET /rewards')
		res.status(200).jsonp(rewards);
	});
};


//GET - Return a Reward with specified ID
exports.findRewardById = function(req, res) {
	rewardsModel
		.findById(req.params.id)
		.exec(function(err, reward) {
	    	if(err) return res.status(500).send(err.message);

	    	console.log('GET /rewards/' + req.params.id);
			res.status(200).jsonp(reward);
		});
};

//POST - Insert a new Reward in the DB
exports.addReward = function(req, res) {
	console.log('POST addReward');
	
	var reward = new rewardsModel({
		language:   req.body.language_code,
		icon: 	  	req.body.icon,
		color: 	  	req.body.color,
		message: 	req.body.message,
		label: 	  	req.body.label
	});
console.log(reward);
	reward.save(function(err, reward) {
		if(err) return res.status(500).send(err.message);
    	res.status(200).jsonp(reward);
	});
};

//PUT - Update a register already exists
exports.updateReward = function(req, res) {
	rewardsModel.findById(req.params.id, function(err, reward) {
		if ( req.body.language ) reward.language   	= req.body.language;
		if ( req.body.icon ) reward.icon    		= req.body.icon;
		if ( req.body.color ) reward.color 			= req.body.color;
		if ( req.body.message ) reward.message 		= req.body.message;
		if ( req.body.label ) reward.timestamp 		= req.body.label;

		reward.save(function(err) {
			if(err) return res.status(500).send(err.message);
      		res.status(200).jsonp(reward);
		});
	});
};

//DELETE - Delete a Reward with specified ID
exports.deleteReward = function(req, res) {
	rewardsModel.findById(req.params.id, function(err, reward) {
		reward.remove(function(err) {
			if(err) return res.status(500).send(err.message);
      		res.status(200);
		})
	});
};