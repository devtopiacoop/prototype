//File: controllers/matches.js
var mongoose 		= require('mongoose');
var config 			= require('../config');
var matchesModel  	= mongoose.model('matches');
var planetsModel  	= mongoose.model('planets');
var usersModel  	= mongoose.model('users');
var gaming		  	= require('./gammification');


//GET - Return all matches in the DB
exports.findAllMatches = function(req, res) {
	matchesModel.find(function(err, matches) {
    	if(err) res.status(500).send(err.message);

    	console.log('GET /matches')
		res.status(200).jsonp(matches);
	});
};

//GET - Return a Match with specified ID
exports.findMatchById = function(req, res) {
	//TODO
};

//POST - Insert a new Match in the DB
exports.startMatch = function(req, res) {
	//TODO
	// Generar array de planetas en función de las apuestas y número de turnos
	console.log('POST');


//****************************************************//
// MOCKINGEANDO
//****************************************************//
var myman = {
  "modal": 1,
  "planets": [
    {
      "spaceships": 0,
      "__v": 0,
      "imagePath": "medium_planet_1",
      "size": 142,
      "type": "medium",
      "points": 300,
      "_id": "5778db80c5a80a4e1b5c09c6"
    },
    {
      "color": 0,
      "spaceships": 0,
      "__v": 0,
      "imagePath": "medium_planet_2",
      "size": 142,
      "type": "medium",
      "points": 305,
      "_id": "5778db89c5a80a4e1b5c09c7"
    },
    {
      "color": 0,
      "spaceships": 0,
      "__v": 0,
      "imagePath": "small_planet_2",
      "size": 89,
      "type": "small",
      "points": 105,
      "_id": "5778db94c5a80a4e1b5c09c8"
    },
    {
      "color": 0,
      "spaceships": 0,
      "__v": 0,
      "imagePath": "small_planet_2",
      "size": 89,
      "type": "small",
      "points": 105,
      "_id": "5778db94c5a80a4e1b5c09c8"
    },
    {
      "color": 0,
      "spaceships": 0,
      "__v": 0,
      "imagePath": "small_planet_1",
      "size": 89,
      "type": "small",
      "points": 100,
      "_id": "5778db9ec5a80a4e1b5c09c9"
    },
    {
      "color": 0,
      "spaceships": 0,
      "__v": 0,
      "imagePath": "medium_planet_1",
      "size": 142,
      "type": "medium",
      "points": 300,
      "_id": "5778db80c5a80a4e1b5c09c6"
    },
    {
      "color": 0,
      "spaceships": 0,
      "__v": 0,
      "imagePath": "small_planet_2",
      "size": 89,
      "type": "small",
      "points": 105,
      "_id": "5778db94c5a80a4e1b5c09c8"
    },
    {
      "color": 0,
      "spaceships": 0,
      "__v": 0,
      "imagePath": "big_planet_1",
      "size": 178,
      "type": "big",
      "points": 500,
      "_id": "5778db6bc5a80a4e1b5c09c5"
    },
    {
      "color": 0,
      "spaceships": 0,
      "__v": 0,
      "imagePath": "small_planet_2",
      "size": 89,
      "type": "small",
      "points": 105,
      "_id": "5778db94c5a80a4e1b5c09c8"
    },
    {
      "color": 0,
      "spaceships": 0,
      "__v": 0,
      "imagePath": "big_planet_2",
      "size": 178,
      "type": "big",
      "points": 505,
      "_id": "5778db64c5a80a4e1b5c09c4"
    }
  ],
  "results": [],
  "players": [
    {
      "_id": "57791bd54b434ec521a96181",
      "spaceships": 100,
      "color": 0,
      "nick": "su_morenito_19",
      "__v": 0
    },
    {
      "_id": "57791be34b434ec521a96183",
      "spaceships": 100,
      "color": 1,
      "nick": "aaaa",
      "__v": 0
    },
    {
      "_id": "57791be64b434ec521a96184",
      "spaceships": 100,
      "color": 2,
      "nick": "bbbb",
      "__v": 0
    },
    {
      "_id": "57791be74b434ec521a96185",
      "spaceships": 100,
      "color": 3,
      "nick": "ccc",
      "__v": 0
    }
  ],
  "timestamp": "2016-07-03T14:06:14.549Z"
 };

res.status(200).send(myman);




/*********************


	// First create user TODO-user signup/signit/auth
	var user = new usersModel({
		nick: 			req.body.nick,
		color:  		config.INDEX_PLAYER,
		spaceships : 	config.NUMBER_OF_SPACESHIPS
	});
	config.INDEX_PLAYER++; //TODO improve color assignment
	user.save(function(err, user) {
		if(err) return res.status(500).send(err.message);
		// Fetch matches waiting for a user
		matchesModel
			.findOne({ 'status': '1', 'bet': req.body.bet}, function(err, match) {
					console.log('search match');

				// Match found -> add user
				if ( !err && match ) {
					console.log("Found match "+match);
					// Add logged user to found match
					match.players.push(user);
					if ( match.players.length == config.PLAYERS_PER_MATCH ) {
						match.status = 0;
						config.INDEX_PLAYER=0;
					}
				} else {
					console.log("No match found");
					console.log("user id:"+user._id)
					// No matches found > open new one
					var match = new matchesModel({
						players : 	[user],
						planets: 	[],
						bet: 		req.body.bet
					});
					console.log("match" + match);
					match.rounds = [[], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], []];
					
				}
				// Add planets
					planetsModel.find(function(err, planets) {
						console.log("Planets....");
						console.log(planets);
						// Fill with random planets
						var selectedPlanets = [];
						for (var i=0;i<config.PLANETS_PER_MATCH;i++) {
							min = 0;
							max = planets.length;
							j = Math.floor(Math.random() * (max - min)) + min; //random in range 0-planets length
							console.log(j);
							planets[j].color = 0;
							selectedPlanets.push(planets[j]);
						}
						if(!err) {
							console.log(selectedPlanets);
							match.planets = selectedPlanets;
							

							match.save(function(err, match) {
								if(err) return res.status(500).send(err.message + " - Saving the match");
								res.status(200).jsonp(match);
							});
						}
					});
				
		});			
	});
**/
	
};

//POST - Check if match is ready //TODO - do it with websocket
exports.isMatchReady = function(req, res) {
	//TODO
	console.log('POST');
	matchesModel
		.find({ "status": 0, "users._id": req.body.user}, function(err, match) {
			res.status(200).send(!err && match?"true":"false");
	});
};

//POST - Check if match is ready //TODO - do it with websocket
exports.playRound = function(req, res) {

var myman = [
///////////////// ROUND 1 ////////////
 {
  "modal": 1,
  "planets": [
    {
      "spaceships": 10,
      "owner" : "57791be74b434ec521a96185",
      "color" : 4,
      "__v": 0,
      "imagePath": "medium_planet_1",
      "size": 142,
      "type": "medium",
      "points": 300,
      "_id": "5778db80c5a80a4e1b5c09c6"
    },
    {
      "color": 0,
      "spaceships": 0,
      "__v": 0,
      "imagePath": "medium_planet_2",
      "size": 142,
      "type": "medium",
      "points": 305,
      "_id": "5778db89c5a80a4e1b5c09c7"
    },
    {
      "color": 2,
      "owner" : "57791be34b434ec521a96183",
      "spaceships": 30,
      "__v": 0,
      "imagePath": "small_planet_2",
      "size": 89,
      "type": "small",
      "points": 105,
      "_id": "5778db94c5a80a4e1b5c09c8"
    },
    {
      "color": 0,
      "spaceships": 0,
      "__v": 0,
      "imagePath": "small_planet_2",
      "size": 89,
      "type": "small",
      "points": 105,
      "_id": "5778db94c5a80a4e1b5c09c8"
    },
    {
      "color": 0,
      "spaceships": 0,
      "__v": 0,
      "imagePath": "small_planet_1",
      "size": 89,
      "type": "small",
      "points": 100,
      "_id": "5778db9ec5a80a4e1b5c09c9"
    },
    {
      "color": 0,
      "owner" : "",
      "spaceships": 0,
      "__v": 0,
      "imagePath": "medium_planet_1",
      "size": 142,
      "type": "medium",
      "points": 300,
      "_id": "5778db80c5a80a4e1b5c09c6"
    },
    {
      "color": 0,
      "spaceships": 0,
      "__v": 0,
      "imagePath": "small_planet_2",
      "size": 89,
      "type": "small",
      "points": 105,
      "_id": "5778db94c5a80a4e1b5c09c8"
    },
    {
      "color": 0,
      "spaceships": 0,
      "__v": 0,
      "imagePath": "big_planet_1",
      "size": 178,
      "type": "big",
      "points": 500,
      "_id": "5778db6bc5a80a4e1b5c09c5"
    },
    {
      "color": 3,
      "owner" : "57791be64b434ec521a96184",
      "spaceships": 20,
      "__v": 0,
      "imagePath": "small_planet_2",
      "size": 89,
      "type": "small",
      "points": 105,
      "_id": "5778db94c5a80a4e1b5c09c8"
    },
    {
      "color": 1,
      "owner" : "57791bd54b434ec521a96181",
      "spaceships": 15,
      "__v": 0,
      "imagePath": "big_planet_2",
      "size": 178,
      "type": "big",
      "points": 505,
      "_id": "5778db64c5a80a4e1b5c09c4"
    }
  ],
  "results": [],
  "players": [
    {
      "_id": "57791bd54b434ec521a96181",
      "spaceships": 85,
      "color": 0,
      "nick": "su_morenito_19",
      "__v": 0
    },
    {
      "_id": "57791be34b434ec521a96183",
      "spaceships": 70,
      "color": 1,
      "nick": "aaaa",
      "__v": 0
    },
    {
      "_id": "57791be64b434ec521a96184",
      "spaceships": 80,
      "color": 2,
      "nick": "bbbb",
      "__v": 0
    },
    {
      "_id": "57791be74b434ec521a96185",
      "spaceships": 90,
      "color": 3,
      "nick": "ccc",
      "__v": 0
    }
  ],
  "timestamp": "2016-07-03T14:06:14.549Z"
 },


 ///////////////// ROUND 2 ////////////
 {
  "modal": 1,
  "planets": [
    {
      "spaceships": 10,
      "owner" : "57791be74b434ec521a96185",
      "color" : 4,
      "__v": 0,
      "imagePath": "medium_planet_1",
      "size": 142,
      "type": "medium",
      "points": 300,
      "_id": "5778db80c5a80a4e1b5c09c6"
    },
    {
      "color": 0,
      "spaceships": 0,
      "__v": 0,
      "imagePath": "medium_planet_2",
      "size": 142,
      "type": "medium",
      "points": 305,
      "_id": "5778db89c5a80a4e1b5c09c7"
    },
    {
      "color": 2,
      "owner" : "57791be34b434ec521a96183",
      "spaceships": 30,
      "__v": 0,
      "imagePath": "small_planet_2",
      "size": 89,
      "type": "small",
      "points": 105,
      "_id": "5778db94c5a80a4e1b5c09c8"
    },
    {
      "color": 0,
      "spaceships": 0,
      "__v": 0,
      "imagePath": "small_planet_2",
      "size": 89,
      "type": "small",
      "points": 105,
      "_id": "5778db94c5a80a4e1b5c09c8"
    },
    {
      "color": 0,
      "spaceships": 0,
      "__v": 0,
      "imagePath": "small_planet_1",
      "size": 89,
      "type": "small",
      "points": 100,
      "_id": "5778db9ec5a80a4e1b5c09c9"
    },
    {
      "color": 0,
      "owner" : "",
      "spaceships": 0,
      "__v": 0,
      "imagePath": "medium_planet_1",
      "size": 142,
      "type": "medium",
      "points": 300,
      "_id": "5778db80c5a80a4e1b5c09c6"
    },
    {
      "owner" : "57791be74b434ec521a96185",
      "color" : 4,
      "spaceships": 5,
      "__v": 0,
      "imagePath": "small_planet_2",
      "size": 89,
      "type": "small",
      "points": 105,
      "_id": "5778db94c5a80a4e1b5c09c8"
    },
    {
      "color": 2,
      "owner" : "57791be34b434ec521a96183",
      "spaceships": 30,
      "__v": 0,
      "imagePath": "big_planet_1",
      "size": 178,
      "type": "big",
      "points": 500,
      "_id": "5778db6bc5a80a4e1b5c09c5"
    },
    {
      "color": 3,
      "owner" : "57791be64b434ec521a96184",
      "spaceships": 20,
      "__v": 0,
      "imagePath": "small_planet_2",
      "size": 89,
      "type": "small",
      "points": 105,
      "_id": "5778db94c5a80a4e1b5c09c8"
    },
    {
      "color": 1,
      "owner" : "57791bd54b434ec521a96181",
      "spaceships": 30,
      "__v": 0,
      "imagePath": "big_planet_2",
      "size": 178,
      "type": "big",
      "points": 505,
      "_id": "5778db64c5a80a4e1b5c09c4"
    }
  ],
  "results": [],
  "players": [
    {
      "_id": "57791bd54b434ec521a96181",
      "spaceships": 70,
      "color": 0,
      "nick": "su_morenito_19",
      "__v": 0
    },
    {
      "_id": "57791be34b434ec521a96183",
      "spaceships": 40,
      "color": 1,
      "nick": "aaaa",
      "__v": 0
    },
    {
      "_id": "57791be64b434ec521a96184",
      "spaceships": 75,
      "color": 2,
      "nick": "bbbb",
      "__v": 0
    },
    {
      "_id": "57791be74b434ec521a96185",
      "spaceships": 80,
      "color": 3,
      "nick": "ccc",
      "__v": 0
    }
  ],
  "timestamp": "2016-07-03T14:06:14.549Z"
 },




 ///////////////// ROUND 3 ////////////
 {
  "modal": 1,
  "planets": [
    {
      "spaceships": 10,
      "owner" : "57791be74b434ec521a96185",
      "color" : 4,
      "__v": 0,
      "imagePath": "medium_planet_1",
      "size": 142,
      "type": "medium",
      "points": 300,
      "_id": "5778db80c5a80a4e1b5c09c6"
    },
    {
      "color": 2,
      "owner" : "57791be34b434ec521a96183",
      "spaceships": 40,
      "__v": 0,
      "imagePath": "medium_planet_2",
      "size": 142,
      "type": "medium",
      "points": 305,
      "_id": "5778db89c5a80a4e1b5c09c7"
    },
    {
      "color": 2,
      "owner" : "57791be34b434ec521a96183",
      "spaceships": 30,
      "__v": 0,
      "imagePath": "small_planet_2",
      "size": 89,
      "type": "small",
      "points": 105,
      "_id": "5778db94c5a80a4e1b5c09c8"
    },
    {
      "color": 1,
      "owner" : "57791bd54b434ec521a96181",
      "spaceships": 20,
      "__v": 0,
      "imagePath": "small_planet_2",
      "size": 89,
      "type": "small",
      "points": 105,
      "_id": "5778db94c5a80a4e1b5c09c8"
    },
    {
      "color": 4,
      "spaceships": 20,
      "owner" : "57791be74b434ec521a96185",
      "__v": 0,
      "imagePath": "small_planet_1",
      "size": 89,
      "type": "small",
      "points": 100,
      "_id": "5778db9ec5a80a4e1b5c09c9"
    },
    {
      "color": 0,
      "owner" : "",
      "spaceships": 0,
      "__v": 0,
      "imagePath": "medium_planet_1",
      "size": 142,
      "type": "medium",
      "points": 300,
      "_id": "5778db80c5a80a4e1b5c09c6"
    },
    {
      "owner" : "57791be74b434ec521a96185",
      "color" : 4,
      "spaceships": 5,
      "__v": 0,
      "imagePath": "small_planet_2",
      "size": 89,
      "type": "small",
      "points": 105,
      "_id": "5778db94c5a80a4e1b5c09c8"
    },
    {
      "color": 3,
      "owner" : "57791be64b434ec521a96184",
      "spaceships": 30,
      "__v": 0,
      "imagePath": "big_planet_1",
      "size": 178,
      "type": "big",
      "points": 500,
      "_id": "5778db6bc5a80a4e1b5c09c5"
    },
    {
      "color": 3,
      "owner" : "57791be64b434ec521a96184",
      "spaceships": 20,
      "__v": 0,
      "imagePath": "small_planet_2",
      "size": 89,
      "type": "small",
      "points": 105,
      "_id": "5778db94c5a80a4e1b5c09c8"
    },
    {
      "color": 1,
      "owner" : "57791bd54b434ec521a96181",
      "spaceships": 30,
      "__v": 0,
      "imagePath": "big_planet_2",
      "size": 178,
      "type": "big",
      "points": 505,
      "_id": "5778db64c5a80a4e1b5c09c4"
    }
  ],
  "results": [],
  "players": [
    {
      "_id": "57791bd54b434ec521a96181",
      "spaceships": 50,
      "color": 0,
      "nick": "su_morenito_19",
      "__v": 0
    },
    {
      "_id": "57791be34b434ec521a96183",
      "spaceships": 0,
      "color": 1,
      "nick": "aaaa",
      "__v": 0
    },
    {
      "_id": "57791be64b434ec521a96184",
      "spaceships": 15,
      "color": 2,
      "nick": "bbbb",
      "__v": 0
    },
    {
      "_id": "57791be74b434ec521a96185",
      "spaceships": 60,
      "color": 3,
      "nick": "ccc",
      "__v": 0
    }
  ],
  "timestamp": "2016-07-03T14:06:14.549Z"
 },




 ///////////////// ROUND 4 ////////////
 {
  "modal": 0,
  "planets": [
    {
      "spaceships": 10,
      "owner" : "57791be74b434ec521a96185",
      "color" : 4,
      "__v": 0,
      "imagePath": "medium_planet_1",
      "size": 142,
      "type": "medium",
      "points": 300,
      "_id": "5778db80c5a80a4e1b5c09c6"
    },
    {
      "color": 2,
      "owner" : "57791be34b434ec521a96183",
      "spaceships": 40,
      "__v": 0,
      "imagePath": "medium_planet_2",
      "size": 142,
      "type": "medium",
      "points": 305,
      "_id": "5778db89c5a80a4e1b5c09c7"
    },
    {
      "color": 2,
      "owner" : "57791be34b434ec521a96183",
      "spaceships": 15,
      "__v": 0,
      "imagePath": "small_planet_2",
      "size": 89,
      "type": "small",
      "points": 105,
      "_id": "5778db94c5a80a4e1b5c09c8"
    },
    {
      "color": 1,
      "owner" : "57791bd54b434ec521a96181",
      "spaceships": 20,
      "__v": 0,
      "imagePath": "small_planet_2",
      "size": 89,
      "type": "small",
      "points": 105,
      "_id": "5778db94c5a80a4e1b5c09c8"
    },
    {
      "color": 4,
      "spaceships": 10,
      "owner" : "57791be74b434ec521a96185",
      "__v": 0,
      "imagePath": "small_planet_1",
      "size": 89,
      "type": "small",
      "points": 100,
      "_id": "5778db9ec5a80a4e1b5c09c9"
    },
    {
      "color": 0,
      "owner" : "",
      "spaceships": 0,
      "__v": 0,
      "imagePath": "medium_planet_1",
      "size": 142,
      "type": "medium",
      "points": 300,
      "_id": "5778db80c5a80a4e1b5c09c6"
    },
    {
      "owner" : "57791be74b434ec521a96185",
      "color" : 4,
      "spaceships": 5,
      "__v": 0,
      "imagePath": "small_planet_2",
      "size": 89,
      "type": "small",
      "points": 105,
      "_id": "5778db94c5a80a4e1b5c09c8"
    },
    {
      "color": 3,
      "owner" : "57791be64b434ec521a96184",
      "spaceships": 30,
      "__v": 0,
      "imagePath": "big_planet_1",
      "size": 178,
      "type": "big",
      "points": 500,
      "_id": "5778db6bc5a80a4e1b5c09c5"
    },
    {
      "color": 3,
      "owner" : "57791be64b434ec521a96184",
      "spaceships": 20,
      "__v": 0,
      "imagePath": "small_planet_2",
      "size": 89,
      "type": "small",
      "points": 105,
      "_id": "5778db94c5a80a4e1b5c09c8"
    },
    {
      "color": 1,
      "owner" : "57791bd54b434ec521a96181",
      "spaceships": 30,
      "__v": 0,
      "imagePath": "big_planet_2",
      "size": 178,
      "type": "big",
      "points": 505,
      "_id": "5778db64c5a80a4e1b5c09c4"
    }
  ],
  "results": [],
  "players": [
    {
      "_id": "57791bd54b434ec521a96181",
      "spaceships": 40,
      "color": 0,
      "nick": "su_morenito_19",
      "__v": 0
    },
    {
      "_id": "57791be34b434ec521a96183",
      "spaceships": 0,
      "color": 1,
      "nick": "aaaa",
      "__v": 0
    },
    {
      "_id": "57791be64b434ec521a96184",
      "spaceships": 0,
      "color": 2,
      "nick": "bbbb",
      "__v": 0
    },
    {
      "_id": "57791be74b434ec521a96185",
      "spaceships": 30,
      "color": 3,
      "nick": "ccc",
      "__v": 0
    }
  ],
  "timestamp": "2016-07-03T14:06:14.549Z"
 },

 


 ///////////////// ROUND 5 ////////////
 {
  "modal": 2,
  "planets": [
    {
      "spaceships": 10,
      "owner" : "57791be74b434ec521a96185",
      "color" : 4,
      "__v": 0,
      "imagePath": "medium_planet_1",
      "size": 142,
      "type": "medium",
      "points": 300,
      "_id": "5778db80c5a80a4e1b5c09c6"
    },
    {
      "color": 2,
      "owner" : "57791be34b434ec521a96183",
      "spaceships": 20,
      "__v": 0,
      "imagePath": "medium_planet_2",
      "size": 142,
      "type": "medium",
      "points": 305,
      "_id": "5778db89c5a80a4e1b5c09c7"
    },
    {
      "color": 2,
      "owner" : "57791be34b434ec521a96183",
      "spaceships": 15,
      "__v": 0,
      "imagePath": "small_planet_2",
      "size": 89,
      "type": "small",
      "points": 105,
      "_id": "5778db94c5a80a4e1b5c09c8"
    },
    {
      "color": 1,
      "owner" : "57791bd54b434ec521a96181",
      "spaceships": 20,
      "__v": 0,
      "imagePath": "small_planet_2",
      "size": 89,
      "type": "small",
      "points": 105,
      "_id": "5778db94c5a80a4e1b5c09c8"
    },
    {
      "color": 4,
      "spaceships": 10,
      "owner" : "57791be74b434ec521a96185",
      "__v": 0,
      "imagePath": "small_planet_1",
      "size": 89,
      "type": "small",
      "points": 100,
      "_id": "5778db9ec5a80a4e1b5c09c9"
    },
    {
      "color": 0,
      "owner" : "",
      "spaceships": 0,
      "__v": 0,
      "imagePath": "medium_planet_1",
      "size": 142,
      "type": "medium",
      "points": 300,
      "_id": "5778db80c5a80a4e1b5c09c6"
    },
    {
      "owner" : "57791be74b434ec521a96185",
      "color" : 4,
      "spaceships": 5,
      "__v": 0,
      "imagePath": "small_planet_2",
      "size": 89,
      "type": "small",
      "points": 105,
      "_id": "5778db94c5a80a4e1b5c09c8"
    },
    {
      "color": 3,
      "owner" : "57791be64b434ec521a96184",
      "spaceships": 30,
      "__v": 0,
      "imagePath": "big_planet_1",
      "size": 178,
      "type": "big",
      "points": 500,
      "_id": "5778db6bc5a80a4e1b5c09c5"
    },
    {
      "color": 1,
      "owner" : "57791bd54b434ec521a96181",
      "spaceships": 20,
      "__v": 0,
      "imagePath": "small_planet_2",
      "size": 89,
      "type": "small",
      "points": 105,
      "_id": "5778db94c5a80a4e1b5c09c8"
    },
    {
      "color": 1,
      "owner" : "57791bd54b434ec521a96181",
      "spaceships": 30,
      "__v": 0,
      "imagePath": "big_planet_2",
      "size": 178,
      "type": "big",
      "points": 505,
      "_id": "5778db64c5a80a4e1b5c09c4"
    }
  ],
  "results": [],
  "players": [
    {
      "_id": "57791bd54b434ec521a96181",
      "spaceships": 0,
      "color": 0,
      "nick": "su_morenito_19",
      "__v": 0
    },
    {
      "_id": "57791be34b434ec521a96183",
      "spaceships": 0,
      "color": 1,
      "nick": "aaaa",
      "__v": 0
    },
    {
      "_id": "57791be64b434ec521a96184",
      "spaceships": 0,
      "color": 2,
      "nick": "bbbb",
      "__v": 0
    },
    {
      "_id": "57791be74b434ec521a96185",
      "spaceships": 0,
      "color": 3,
      "nick": "ccc",
      "__v": 0
    }
  ],
  "timestamp": "2016-07-03T14:06:14.549Z"
 },
];

res.status(200).send(myman[req.body.index]);

	// body expected:
	// {
	//  match: _id,
	// 	user: _id,
	// 	spaceships: N,
	// 	planet: _id
	// }
	//
/**********************************************************************************
MOCKED POWER
**********************************************************************************
	console.log('POST');
	// Find match
	matchesModel
		.findById(req.body.match)
		.exec(function(err, match) {
			if (err) return res.status(500).send("No match found");
			// Add play round to match
			console.log("match round =====> "+match.round);
			console.log(match.rounds);
			var ppp = match.rounds;
			ppp[match.round].push({ 
				user: req.body.user,
				spaceships: req.body.spaceships,
				planet: req.body.planet,
			});
			match.rounds = ppp;
					
			console.log(match);
			match.save(function(err, match) {
				if(err) return res.status(500).send(err.message + " - Saving the match");
					// Wait for other players
					// For now with countdown??? TODO-Websockets
					console.log("Current round match::"+match);
					this.setTimeout(function(){
						// End round -> reload match
						matchesModel
							.findById(req.body.match)
							.exec(function(err, match) {
								if (err) return res.status(500).send("No match found");
								/////////////////////////////////////////////////////////////////////////
								//                          GAMMIFICATION CORE                         //
								/////////////////////////////////////////////////////////////////////////
								console.log("reloaded match =====> "+match);
								console.log("Lets play...");
								// Run all planets in match
								for (var i=0; i<match.planets.length;i++) {
									console.log("********** Planet "+i+" *****"+match.round+"*******");
									// Search users fighting for this planet (index i)
									var ro = match.rounds[match.round];
									console.log(ro);
									var fightingPlayers = ro.filter(function( round ) {
									  return round.planet == i;
									});
									console.log("Fighting players :: " + fightingPlayers);
									if (fightingPlayers.length > 0) {
										// Check if planet has owner (who has priority to add spaceships)
										if (match.planets[i].hasOwnProperty('owner') ) {
											console.log(".... Planet owned .... ");
											// Check if owner is playing for this planet
											var owningPlayer = match.rounds[match.round].filter(function( round ) {
											  return round.user == match.planets[i].owner;
											});
											console.log("Owner is playing for its own planet... reforce");
											if ( owningPlayer.length > 0 ) {
												// Reforce defense
												match.planets[i].spaceships += owningPlayer[0].spaceships;
												console.log("Defense reforced...");

												// Remove player from round
												for (var k=0;k<match.rounds[match.round].lenght;k++) {
													if (match.rounds[match.round][k].user == owningPlayer.user) {
														match.rounds[match.round].splice(k, 1);
          												break;
													}
												}
											} else {
												// has owner but players are new

											}

										} else {
											console.log(".... Planet empty .... ");
										}
										
									} 
									// Again: Search users fighting for this planet (index i) after removing possible owner
									//TODO better
									var fightingPlayers = match.rounds[match.round].filter(function( round ) {
									  	return round.planet == i;
									});
									console.log("New fightingPlayers..." +fightingPlayers );

									var winners = [];
									// 1 players round
									if ( fightingPlayers.length == 1 ) {
										console.log("Only one player");
										winners.push(fightingPlayers[0]);
									} else if ( fightingPlayers.length == 2 ) {
										//// 2 players round
										console.log("2 players round");
										if ( fightingPlayers[0].spaceships > fightingPlayers[1].spaceships ) {
											fightingPlayers[0].spaceships-=fightingPlayers[1].spaceships;
											winners.push(fightingPlayers[0]);
										} else if ( fightingPlayers[0].spaceships < fightingPlayers[1].spaceships) {
											fightingPlayers[1].spaceships-=fightingPlayers[0].spaceships;
											winners.push(fightingPlayers[1]);
										}
									} else if ( fightingPlayers.length > 2 ) { // more than 2
										// the lowest loses all
										// find the biggest
										console.log("more than 2 players round");
										var maxSpaceships = 0;
										var toRemove = 0;
										// Fight -> the biggest bet (spaceships) wins, if equals -> both lose
										for (var k=0;k<fightingPlayers.length;k++) {
											if ( fightingPlayers[k].spaceships > maxSpaceships ) {
												toRemove = maxSpaceships; //save value to remove at the end
												maxSpaceships = fightingPlayers[k].spaceships;
												if (winners.length>0) {
													// remove old winner
													winners = new Array(fightingPlayers[k]);
												}
											} else if ( fightingPlayers[k].spaceships == maxSpaceships ) {
												// equals > add to array
												winners.push(fightingPlayers[k]);
											}
										}
										if ( winners.length > 1 ) {
											// more than one equals -> all lose
											winners = [];
										} else if (winners.length == 1) {
											winners[0].spaceships -= toRemove;
										}
									}
									console.log("Fight with planet ... ?");
									console.log("Winners :: "+winners);
									// Winners it should be only one
									if ( winners.length > 0 ){
										if (match.planets[i].hasOwnProperty('owner') ) {
											console.log(".... Planet owned .... ");
											// Fight
											if ( match.planets[i].spaceships > winners[0].spaceships ) {
												match.planets[i].spaceships-=winners[0].spaceships;
											} else if (match.planets[i].spaceships == winners[0].spaceships ) {
												match.planets[i].spaceships=0;
												match.planets[i].owner = null;
											} else {
												match.planets[i].owner = winners[0].user;
												match.planets[i].spaceships = winners[0].spaceships-match.planets[i].spaceships;
											}
											
										} else {
											console.log(".... Planet empty .... Conquered ");
											match.planets[i].owner = winners[0].user;
											match.planets[i].spaceships = winners[0].spaceships;
											
										}
										// Get owner data
										console.log("The winner is:::::::::"+winners[0]);
										console.log(winners[0].user);
										for (var r=0;r<match.players.length;r++) {
											console.log(".XXXXXXX." + r + "...."+match.players[r]._id);
											if (match.players[r]._id == winners[0].user ) {
												match.planets[i].color =match.players[r].color
												match.players[r].spaceships-=winners[0].spaceships;
											}
										}

										var p = match.players.filter(function( player ) {
											return player._id == winners[0].user;
										});
										console.log("...Coloring planet..." + p);
										if ( p.length > 0 ) match.planets[i].color = p[0].color;
									}
									
								}

								// Clean round
								//match.rounds = [];

								if ( match.round == 1 ) {
									// Last round
									// Resolve game
									match.status = -1;
									for (var j=0; j<match.planets.length;j++) {
										if ( !(match.planets[j].owner in match.results) ) {
											match.results[match.planets[j].owner] = match.planets[j].points;
										} else {
											match.results[match.planets[j].owner] += match.planets[j].points;
										}
									}
								}
								match.round--;
								match.save(function(err,match){
										// Add color to planet para el puto juan
										for ( var i=0;i<match.planets.length;i++) {
											if (match.planets[i].hasOwnProperty('owner')) {
												// Get owner data
												var p = match.players.filter(function( player ) {
												  return player._id == match.planets[i].owner;
												});

												match.planets[i].color = p.color;
											}
										}
										res.status(200).jsonp(match);
								});
							});
					},1000*(config.TIME_PER_ROUND)); // at most we have to wait the full round1
				});
	});

	***********************************************************************************/
};

//POST - Insert a new Match in the DB
exports.endMatch = function(req, res) {
	//TODO

};


//PUT - Update a register already exists
exports.updateMatch = function(req, res) {
	
};