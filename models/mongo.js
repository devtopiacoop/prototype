var mongoose  = require('mongoose'),  
    random    = require('mongoose-simple-random'),
    Schema    = mongoose.Schema; // create instance of Schema
var config    = require('../config');

// Connection to DB
mongoose.connect('mongodb://127.0.0.1/prototype', function(err, res) {
  if(err) {
    console.log('ERROR: connecting to Database. ' + err);
    throw err;
  }
  console.log('Connected to Database');
});


// create users schema and oAuth schema
var usersSchema = new Schema({  
  nick:           { type: String },
  color:          { type: Number },
  spaceships:     { type: Number },
  //email:          { type: String ,  unique: true, required: true},
  //hashedPassword: { type: String,  required: true },
  //settings:       { type: String },
  //active:         { type: Boolean , default: true },
  //created:        { type: Date, default: Date.now },
  //score:          { type: Number, default: 0 , index: true},
  //ranking:        { type: Number, default: 1 , index: true },
  //rewards:        [{ type: Schema.Types.ObjectId, ref: 'rewards' }]
});

usersSchema.methods.encryptPassword = function(password) {
    return require('crypto')
                          .createHash('sha1')
                          .update(password)
                          .digest('base64')
};

usersSchema.methods.checkPassword = function(password) {
    return this.encryptPassword(password) === this.hashedPassword;
};

usersSchema.methods.setInitialRanking = function() {
    return this.count(); // TODO - Set initial ranking. For now the last one
};

// create planets schema
var planetsSchema = new Schema({  
  points:       { type: Number },
  size:         { type: Number }, // size in pixels
  type:         { type: String }, // big, medium, small
  color:        { type: Number, default: 0 },
  imagePath:    { type: String }, 
  status:       { type: Number }, // with value only in match
  owner:        { type: Schema.Types.ObjectId, ref: 'users' }, // with value only in match
  spaceships:   { type: Number, default: 0 }
});
planetsSchema.plugin(random);

// create matches schema
var matchesSchema = new Schema({  
  timestamp:    { type : Date, default: Date.now },
  players:      [Schema.Types.Mixed],
  results:      [Schema.Types.Mixed],
  planets:      [Schema.Types.Mixed],
  bet:          { type: Number }, // Rest of params depends on bet value
  round:        { type: Number , default: config.NUMBER_OF_ROUNDS}, // Rest of params depends on bet value
  status:       { type: Number , default: 1 }, // 0: closed, 1: open, -1: over
  rounds:       [ Schema.Types.Mixed ]
});

// create rewards schema
var rewardsSchema = new Schema({  
  icon:         { type: String },
  message:      { type: String },
  color:        { type: String },
  label:        { type: String }
});

// create model if not exists.
module.exports = mongoose.model('users', usersSchema);  
module.exports = mongoose.model('planets', planetsSchema);  
module.exports = mongoose.model('matches', matchesSchema);  
module.exports = mongoose.model('rewards', rewardsSchema);  
