###Game Weekend

**Inspiración**: Para la realización de esta idea nos hemos basado en el juego de apuestas del poker. 

**Idea**: Juego de conquista de planetas. Conseguir obtener el mayor número de puntos conquistando los planetas que nos rodean y combatiendo contra nuestros 3 contrincantes. AL finalizar la batalla se lleva el dinero el contrincante con más puntos. En caso de empate se reparte el premio.

**Reglamento**

 - Se dispone de un número limitado de naves para conquistar planetas. 
 - La batalla dura un número limitado de turnos y en cada turno tienes un tiempo limitado para realizar la elección que te puede llevar a ganar puntos.
 - Los planetas a conquistar tienen un valor, cuanto mayor sea el valor del planeta conquistado más puntos se obtienen.
 - En cada turno los jugadores sólo pueden lanzar un ataque a un planeta indicando el planeta y el número de naves a enviar.
 - Si un planeta es conquistado por un jugador se indicará que jugador lo ha conquistado y el número de naves que ha conseguido depositar en el planeta.
 - En caso de que un jugador quiera conquistar un planeta que ya esta conquistado por otro jugador tendrá que superar en naves al jugador que está en ese planeta. En caso de superarlo le quitará el planeta a ese jugador colocando en dicho planeta la diferencia entre las naves que envía y las que tenía en el planeta su contrincante. En caso de empatar el planeta quedará de nuevo libre perdiendo los dos jugadores esas naves. En caso de no superar el número de naves el planeta seguirá conquistado por el mismo jugador pero este recibirá un desgaste perdiendo tantas naves como su contrincante haya enviado.
 - Un planeta conquistado por un jugador puede ser fortalecido por este jugador mandando más naves al planeta. En caso de que, en un mismo turno, se envíen refuerzos a un planeta y se realiza un ataque a dicho planeta primero se realizará el refuerzo (tiene prioridad al tener el planeta conquistado) y luego se realizará el ataque.
 - En caso de que dos jugadores, en un mismo turno, compitan por un planeta libre lo conseguirá el que más naves envíe a este planeta dejando en él la diferencia entre los enviados por el ganador y los enviados por el perdedor (que perderá las naves enviadas) del combate. En caso de empate los dos jugadores perderán sus naves y el planeta continuará libre.
 - Si tenemos dos jugadores, en un mismo turno, compitiendo por un planeta ya conquistado por otro jugador, primero se realizará el combate entre ellos como en el punto anterior, las naves supervivientes del jugador ganador competirán con las naves que tiene el jugador ha conquistado dicho planeta previamente (este jugador puede haber reforzado estas naves en ese mismo turno), *ver punto 6* .
 - En caso de que sean tres jugadores compitiendo por un mismo planeta en un mismo turno, el que menos naves envíe quedará derrotado y sin naves, los dos jugadores restantes verán reducidas su número de naves a partes iguales en relación al jugador derrotado y se efectuará un combate a dos como en el punto anterior. El ganador tendrá derecho a conquistar el planeta o a luchar con su inquilino en caso de que este ya esté conquistado. En caso de triple empate todos los jugadores perderán sus naves y el planeta quedará en el mismo estado en el que estuviese.

**Modos de juego**: Existen dos modos de juego:

 - *Modo campeonato*: El jugador realiza una apuesta única para competir en un campeonato, dependiendo del número de jugadores el campeonato tendrá más o menos fases, en cada fase 4 jugadores competirán en un combate y sólo el ganador pasará a la siguiente fase (en caso de empate se realizará un desempate). El ganador de la fase final se llevará la suma total de todas las apuestas del campeonato (menos la comisión por organización correspondiente). En esta fase final si se realiza un empate el premio será repartido.
 - *Modo sit and go*: Son combates aislados de cuatro jugadores que realizan una misma apuesta (1€, 5€, 10€.......) dependiendo del dinero apostado el combate tendrá más o menos turnos, cada jugador tendrá más o menos naves y podrá conquistar más o menos planetas. El ganador del combate se llevará la suma total de las cuatro apuestas (menos la comisión por organización correspondiente). En caso de empate se repartirá el premio.  

     

