// config.js
module.exports = {  
  TOKEN_SECRET			: process.env.TOKEN_SECRET || "madafaka",
  NUMBER_OF_PLAYERS		: 4,
  PLANETS_PER_MATCH		: 10,
  PLAYERS_PER_MATCH		: 4,
  NUMBER_OF_SPACESHIPS	: 100,
  NUMBER_OF_ROUNDS		: 10,
  TIME_PER_ROUND		: 10, //in seconds
  COLORS				: [ "#ccc", "#ff0000", "#00ff00", "#0000ff"],
  INDEX_PLAYER			: 0
};