var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var methodOverride  = require("method-override");

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.engine('html', require('ejs').renderFile);
app.set('view engine', 'html');


// Middlewares
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(methodOverride());

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// Import Models and controllers
var models                = require('./models/mongo');
var usersController       = require('./controllers/users');
var planetsController     = require('./controllers/planets');
var matchesController     = require('./controllers/matches');
var rewardsController     = require('./controllers/rewards');

// Auth controllers
var auth = require('./libs/auth');  
var middleware = require('./libs/middleware');

// Index Route
var router = express.Router();
router.get('/', function(req, res) {
  res.render('index', { title: 'Express' });
});
app.use(router);

// Auth routes
router.post('/auth/signup', auth.emailSignup);  
router.post('/auth/login', auth.emailLogin);


/**********************************************************************/
/*                            API routes                              */
/**********************************************************************/

///// 
/////     USERS
/////
var users = express.Router();

users.route('/users')
  .get(middleware.ensureAuthenticated, usersController.findAllUsers)
  .post(middleware.ensureAuthenticated, usersController.addUser);
users.route('/users/:id')
  .get(middleware.ensureAuthenticated, usersController.findUserById)
  .put(middleware.ensureAuthenticated, usersController.updateUser)
  .delete(middleware.ensureAuthenticated, usersController.deleteUser);
users.route('/userScore/:id')
  .get(middleware.ensureAuthenticated, usersController.findUserScore)
  .put(middleware.ensureAuthenticated, usersController.updateUser);
users.route('/userScore')
  .get(middleware.ensureAuthenticated, usersController.findUserScore)
  .put(middleware.ensureAuthenticated, usersController.updateUser);

app.use(users);

///// 
/////     MATCHES
/////
var matches = express.Router();

matches.route('/matches')
  .get(middleware.ensureAuthenticated, matchesController.findAllMatches)
matches.route('/matches/:id')
  .get(middleware.ensureAuthenticated, matchesController.findMatchById)
  .put(middleware.ensureAuthenticated, matchesController.updateMatch)
matches.route('/isMatchReady')
  .post(matchesController.isMatchReady);
matches.route('/startMatch')
  .post(matchesController.startMatch);
  //.post(middleware.ensureAuthenticated, matchesController.startMatch);
matches.route('/endMatch/:id')
  .post(middleware.ensureAuthenticated, matchesController.endMatch);
matches.route('/playRound')
  .post(matchesController.playRound);

app.use(matches);


///// 
/////     PLANETS
/////
var planets = express.Router();

planets.route('/planets')
  .get(middleware.ensureAuthenticated, planetsController.findAllPlanets)
  .post(middleware.ensureAuthenticated, planetsController.addPlanet);
planets.route('/randomPlanets')
  .get(middleware.ensureAuthenticated, planetsController.findRandomPlanets);
planets.route('/planets/:id')
  .get(middleware.ensureAuthenticated, planetsController.findPlanetById)
  .put(middleware.ensureAuthenticated, planetsController.updatePlanet)
  .delete(middleware.ensureAuthenticated, planetsController.deletePlanet);
  

app.use(planets);



// Start server
app.listen(3000, function() {
  console.log("Node server running on http://localhost:3000");
});


module.exports = app;
