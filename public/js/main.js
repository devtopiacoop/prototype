WebFontConfig = {
    active: function() {
    },
    google: {
        families: ["Passion One","Bungee"]
    }

};
var core = {
    mocked : 0,
    dieeeeee : false,
    game : null,
    score : 200,
    planet_pos : [],
    toto : 0,
    shipsToSend : "",
    selected : {ui:{index:null}},
    countdown : 10,
    countdownui : null,
    rounds : 1,
    roundsui : null,
    width:1440,
    height:900,
    spaceshipsui : null,
    updateSpaceships : function(){
        var style = { font: "50px Passion One", fill: "#fff", align: "center"};
        if(!core.spaceshipsui){
            core.spaceshipsui = core.game.add.text(490, 820, core.player.spaceships, style);
        }else{
            core.spaceshipsui.setText(core.player.spaceships);
        }
    },
    events : {
        textInput : function(char){
            if(core.selected){
                if(char === "d"){
                    //delete
                    core.shipsToSend = "0";
                }else{
                    core.shipsToSend += char;
                }
                    core.selected.tooltipText.setText(parseInt(core.shipsToSend));
            }
        },
        planetClick : function(sprite){
            sprite.frame = 1;
        },
        uiClick : function(sprite){
            core.shipsToSend = "";
            core.utils.hideAllTooltips();
            core.utils.drawTooltip(sprite.index);

            if(sprite.frame == 2){
                sprite.frame = 1;
                sprite.old = 1;
            }else{
                sprite.frame = 2;
                sprite.old = 2;
            }
        },
        uiOver : function(sprite){
            sprite.old = sprite.frame;
            sprite.frame = 1;
        },
        uiOut : function(sprite){
            sprite.frame = sprite.old;
        }
    },
    phaser : {
        preload : function () {
            core.game.load.script("webfont", "//ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js");

            core.game.load.spritesheet('small_planet_1', 'img/small_planet_1.png', 89, 89, 5);
            core.game.load.spritesheet('medium_planet_1', 'img/medium_planet_1.png', 142, 142, 5);
            core.game.load.spritesheet('big_planet_1', 'img/big_planet_1.png', 178, 178, 5);

            core.game.load.spritesheet('small_planet_2', 'img/small_planet_2.png', 89, 89, 5);
            core.game.load.spritesheet('medium_planet_2', 'img/medium_planet_2.png', 142, 142, 5);
            core.game.load.spritesheet('big_planet_2', 'img/big_planet_2.png', 178, 178, 5);

            core.game.load.spritesheet('small_planet_ui', 'img/small_planet_ui.png', 89, 89, 3);
            core.game.load.spritesheet('medium_planet_ui', 'img/medium_planet_ui.png', 142, 142, 3);
            core.game.load.spritesheet('big_planet_ui', 'img/big_planet_ui.png', 178, 178, 3);

            core.game.load.image('ship', 'img/enviar_naves_icon.png', 41, 49);
            core.game.load.image("background", "img/game_bg.png");

            core.game.load.image("hud_top", "img/hud_top.png");
            core.game.load.image("hud_footer", "img/hud_footer.png");

            core.game.load.image('modal_1', 'img/window_win.png');
            core.game.load.image('modal_0', 'img/window_lost.png');
            core.game.load.image('modal_2', 'img/window_end.png');

            core.game.load.image('logo', 'img/logo_inside.png');
        },
        create : function(){
            core.game.add.tileSprite(0, 0, core.width, core.height, "background");
            core.game.add.sprite(20,20,"logo");
            core.game.input.keyboard.addCallbacks(this, null, null, core.events.textInput);




        },
        update : function(){

        }
    },
    net : {
        getGrid : function(grids){
            var index = 0;
            var grid = grids;

            //Paint the game
            while(core.data.planets.length < grid.planets.length){
                var newPlanet = {};
                if(core.planet_pos.hasOwnProperty(index)){
                    newPlanet.x = core.planet_pos[index][0];
                    newPlanet.y = core.planet_pos[index][1];
                }else{
                    newPlanet.x = core.utils.random(100,core.width-100);
                    newPlanet.y = core.utils.random(100,core.height-300);
                }


                newPlanet.size  = grid.planets[index].size;
                newPlanet.image = grid.planets[index].imagePath;
                newPlanet.color = grid.planets[index].color;
                newPlanet.type  = grid.planets[index].type;


                if(!core.utils.planets.isColliding(newPlanet) || core.utils.count > 2000000){

                    core.planet_pos.push([newPlanet.x,newPlanet.y]);
                    var planet = core.game.add.sprite(newPlanet.x, newPlanet.y, newPlanet.image);
                        planet.frame = grid.planets[index].color;
                        planet.index = core.data.planets.length-1;

                    newPlanet.sprite = planet;
                    core.data.planets.push(newPlanet);

                    //Add listener
                    planet.inputEnabled = true;
                    planet.events.onInputDown.add(core.events.planetClick, this);

                    var ui = core.game.add.sprite(newPlanet.x, newPlanet.y, grid.planets[index].type+"_planet_ui");
                        ui.index = core.data.planets.length-1;
                    newPlanet.ui = ui;

                    //Add listener
                    ui.inputEnabled = true;
                    ui.events.onInputDown.add(core.events.uiClick, this);
                    ui.events.onInputOver.add(core.events.uiOver, this);
                    ui.events.onInputOut.add(core.events.uiOut, this);

                    if(grid.planets[index].spaceships > 0){
                        core.utils.drawLandedShips(index,grid.planets[index].spaceships);
                    }

                    var style = { font: "14px Arial", fill: "#222", align: "center"};
                    core.game.add.text(newPlanet.x+newPlanet.size/2, newPlanet.y+newPlanet.size/2, index, style);

                    index++;
                }
                core.utils.count++;
            }
        },
        postMove : function(){
            /*
            data = {
                "match" : core.data.match,
                "user" : core.player._id,
                "spaceships" : core.shipsToSend,
                "planet" : core.selected.ui.index
            };
            */
           data = {
               index : core.mocked
           };
           core.mocked++;
           console.log(core.mocked+" mocked round");
            $.post("http://localhost:3000/playRound",data,
                function(result){
                    $.each(core.data.planets,function(i,e){
                        core.game.world.remove(e.ui);
                        core.game.world.remove(e.sprite);
                        core.utils.hideAllTooltips();
                    });
                    core.data.player = core.utils.findPlayer(result.players);

                    core.data.planets=[];
                    core.net.getGrid(result);
                    core.utils.showModal(+result.modal);
                    core.updateSpaceships();
                }
            );
        }
    },

    data : {
        canvas : null,
        ctx : null,
        tag: "GameWeekend",
        planets : []
    },
    utils : {
        showModal : function(whichModal) {
            if(whichModal === 2){
                core.dieeeeee = true;
            }
            core.modal = core.game.add.sprite(core.width/2-273,core.height/2-280,"modal_"+whichModal);
        },
        hideModal : function(){
            core.game.world.remove(core.modal);
        },
        padDigits : function(number, digits) {
            return Array(Math.max(digits - String(number).length + 1, 0)).join(0) + number;
        },
        findPlayer : function(players){
            $.each(players,function(i,e){
                if(e.nick == core.nick){
                    core.player = e;
                }
            });
        },
        drawScore : function(text){
            var style = { font: fontSizes[planet.type]+"px Passion One", fill: "#fff", align: "center"};
            text = core.game.add.text(planet.x, planet.y, 50, style);
        },
        drawLandedShips : function(planet_index,ships){
            if(core.data.planets[planet_index].spaceshipsuitext){
                core.game.world.remove(core.data.planets[planet_index].spaceshipsuitext);
            }
            if(core.data.planets[planet_index].spaceshipsuishadow){
                core.game.world.remove(core.data.planets[planet_index].spaceshipsuishadow);
            }
            var planet = core.data.planets[planet_index];
            var fontSizes = {"small":28,"medium":42,"big":56};
            var style = { font: fontSizes[planet.type]+"px Passion One", fill: "#fff", align: "center"};
            var styleShadow = { font: fontSizes[planet.type]+"px Passion One", fill: "#000", align: "center"};
            shadow = core.game.add.text(planet.x, planet.y, ships, styleShadow);
            shadow.alpha = 0.5;
            text = core.game.add.text(planet.x, planet.y, ships, style);


            text.anchor.set(0.5);
            shadow.anchor.set(0.5);

            text.x = Math.floor(planet.x + planet.size / 2);
            text.y = Math.floor(planet.y + planet.size / 2)+20;

            shadow.x = Math.floor(planet.x + planet.size / 2)+6;
            shadow.y = Math.floor(planet.y + planet.size / 2)+26;

            planet.spaceshipsuitext = text;
            planet.spaceshipsuishadow = shadow;
        },
        hideAllTooltips : function(){
            for(i=0;i<core.data.planets.length;i++){
                core.game.world.remove(core.data.planets[i].tooltipText);
                core.game.world.remove(core.data.planets[i].tooltipShip);
                core.data.planets[i].ui.frame = 0;
            }
            //core.data.planets=[];
        },
        drawTooltip : function(planet_index){

            var planet = core.data.planets[planet_index];
            core.selected = planet;
            var style = { font: "59px Passion One", fill: "#FFD900", align: "center"};
            text = core.game.add.text(planet.x, planet.y-20, 0, style);
            text.anchor.set(0.5);

            var center = Math.floor(planet.x + planet.size / 2);
            text.x = center+14;
            var ship = core.game.add.sprite(center-50, planet.y-50, 'ship');
            text.y = planet.y-20;

            planet.tooltipText = text;
            planet.tooltipShip = ship;
        },
        count : 0,
        random : function(min, max){
            return Math.floor(Math.random() * (max - min + 1)) + min;
        },
        log : function(msg){
            console.log(core.data.tag+" :: "+msg);
        },
        planets : {
            isColliding : function(planet){
                //Is it inside grid completely?
                if( planet.x+planet.size/2 > core.width - 100
                  || planet.y+planet.size/2 > core.height - 100
                  || planet.x-planet.size/2 < 50
                  || planet.y-planet.size/2 < 50){
                      return true;
                  }
                closest = 10e9;
                closestPlanet = planet;
                for(i=0;i<core.data.planets.length;i++){
                    var a = core.data.planets[i].x - planet.x;
                    var b = core.data.planets[i].y - planet.y;
                    var c = Math.sqrt( a*a + b*b );

                    if(c < closest){
                        closest = c;
                        closestPlanet = core.data.planets[i];
                    }
                }
                if(closest < closestPlanet.size+100){
                    return true;
                }
                core.utils.count = 0;
                return false;
            }
        }
    },
    init : function(){
        $(function(){
            core.game = new Phaser.Game(core.width, core.height, Phaser.AUTO, '',
            {
                preload: core.phaser.preload,
                create: core.phaser.create,
                update: core.phaser.update
            }),

            $("#play").click(function(){
                core.nick = $("#nick").val();

                core.game.add.sprite(core.width/2-130,-3,"hud_top");
                core.game.add.sprite(core.width/2-(1144/2),core.height-130,"hud_footer");

                //Coutndown
                var style = { font: "48px Passion One", fill: "#fff", align: "center"};
                core.countdownui = core.game.add.text(760, 24, core.countdown, style);

                //Rounds
                var style2 = { font: "48px Passion One", fill: "#fff", align: "center"};
                core.roundsui = core.game.add.text(610, 24, core.utils.padDigits(core.rounds,2)+"/05", style2);

                $.post("http://localhost:3000/startMatch",//"http://dev.devtopia.coop:3000/startMatch",
                      $("#form").serialize(),
                      function(response){
                          $("#login").fadeOut(400);
                          core.data.player = core.utils.findPlayer(response.players);
                          core.net.getGrid(response);
                          core.data.match = response._id;

                          setInterval(function(){
                              if(!core.dieeeeee){
                                  core.updateSpaceships();
                                  if(core.countdown <= 0){
                                      core.countdown = 10;
                                  }
                                  if(core.toto%2===0){
                                      core.countdownui.setText(core.utils.padDigits(core.countdown,2));
                                      core.countdown--;
                                  }else{
                                      core.countdownui.setText(10);
                                  }
                              }
                          }, 1000);

                          setInterval(function(){
                              if(!core.dieeeeee){
                                  if(core.toto%2===0){
                                      core.net.postMove();

                                  }else{
                                      core.utils.hideModal();
                                      core.rounds++;
                                      core.roundsui.setText(core.utils.padDigits(core.rounds,2)+"/05");
                                  }
                                  core.countdown = 10;
                                  core.toto++;
                              }
                          }, 10000);
                      });
            });



            setTimeout(function(){
                //$("#play").click();
            },500);



        });
    }
};
//Start
core.init();
